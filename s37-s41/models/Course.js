import { Schema, model } from 'mongoose';

const courseSchema = new Schema({
  name: {
    type: String,
    required: [true, 'Course name is required!'],
  },
  description: {
    type: String,
    required: [true, 'Description is required!'],
  },
  price: {
    type: Number,
    required: [true, 'Price is required!'],
  },
  isActive: {
    type: Boolean,
    default: true,
  },
  createdOn: {
    type: Date,
    default: new Date(),
  },
  enrollees: [
    {
      userId: {
        type: String,
        required: [true, 'User ID is required!'],
      },
      enrolledOn: {
        type: Date,
        default: new Date(),
      },
    },
  ],
});

export default model('Course', courseSchema);
