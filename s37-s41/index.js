import express, { json, urlencoded } from 'express';
import { connect } from 'mongoose';
import cors from 'cors';
import userRoute from './routes/userRoute.js';
import courseRoute from './routes/courseRoute.js';

const app = express();

const database =
  'mongodb+srv://renzodsantos:admin123@wdc028-course-booking.gaiqe0t.mongodb.net/courseBookingAPI';
const port = process.env.PORT || 4000;

connect(database, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => {
    console.log('Connected to MongoDB');
    app.listen(port, () => console.log(`Listening on port ${port}...`));
  })
  .catch(err => console.error(err.message));

app.use(cors());
app.use(json());
app.use(urlencoded({ extended: true }));

app.use('/users', userRoute);
app.use('/courses', courseRoute);
app.all('*', (req, res) => res.status(404).send('Page not found.'));

app.use((err, req, res, next) => {
  const { message, stack } = err;
  res.status(res.statusCode || 500).json({ message, stack });
});
