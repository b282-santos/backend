import { Router } from 'express';
import * as auth from '../utils/auth.js';
import * as courseController from '../controllers/courseController.js';

const router = Router();

router.post('/create', auth.verify, courseController.addCourse);
router.get('/all', courseController.getAllCourses);
router.get('/active', courseController.getAllActive);
router.get('/:courseId', courseController.getCourse);
router.put('/:courseId', auth.verify, courseController.updateCourse);
router.patch('/:courseId/archive', auth.verify, courseController.archiveCourse);

export default router;
