import { Router } from 'express';
import * as auth from '../utils/auth.js';
import * as userController from '../controllers/userController.js';

const router = Router();

router.get('/details', auth.verify, userController.getProfile);
router.post('/checkEmail', userController.checkEmailExists);
router.post('/register', userController.registerUser);
router.post('/login', userController.loginUser);
router.post('/enroll', auth.verify, userController.enroll);

export default router;
