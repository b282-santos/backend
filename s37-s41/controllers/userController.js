import { isValidObjectId } from 'mongoose';
import { genSalt, hash, compare } from 'bcrypt';
import User from '../models/User.js';
import Course from '../models/Course.js';
import * as auth from '../utils/auth.js';

export const checkEmailExists = async (req, res, next) => {
  try {
    const user = await User.findOne({ email: req.body.email });
    if (!user) {
      res.status(400);
      throw new Error('User not found.');
    }
    res.status(200).json(user);
  } catch (err) {
    next(err);
  }
};

export const registerUser = async (req, res, next) => {
  try {
    const salt = await genSalt(10);
    const password = await hash(req.body.password, salt);
    const newUser = await User.create({ ...req.body, password });
    res.status(201).send(true);
  } catch (err) {
    next(err);
  }
};

export const loginUser = async (req, res, next) => {
  try {
    const user = await User.findOne({ email: req.body.email });
    if (!user) {
      res.status(400);
      throw new Error('Invalid email or password.');
    }

    const validPassword = await compare(req.body.password, user.password);
    if (!validPassword) {
      res.status(400);
      throw new Error('Invalid email or password.');
    }

    res.status(200).json({ access: auth.createAccessToken(user) });
  } catch (err) {
    next(err);
  }
};

export const getProfile = async (req, res, next) => {
  try {
    const { id } = auth.decode(req.headers.authorization);
    const user = await User.findById(id);
    user.password = '';
    res.status(200).send(user);
  } catch (err) {
    next(err);
  }
};

export const enroll = async (req, res, next) => {
  try {
    const { id: userId } = auth.decode(req.headers.authorization);
    const { courseId } = req.body;

    if (!courseId) {
      res.status(400);
      throw new Error('Course ID is required.');
    }

    if (!isValidObjectId(courseId)) {
      res.status(400);
      throw new Error('Invalid Course ID.');
    }

    const user = await User.findById(userId);
    const course = await Course.findById(courseId);

    if (!user || !course) {
      res.status(400);
      throw new Error('Course enrollment failed.');
    }

    user.enrollments.push({ courseId });
    await user.save();
    course.enrollees.push({ userId });
    await course.save();
    res.status(200).send(true);
  } catch (err) {
    next(err);
  }
};
