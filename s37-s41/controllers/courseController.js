import Course from '../models/Course.js';
import * as auth from '../utils/auth.js';

export const addCourse = async (req, res, next) => {
  try {
    const { isAdmin } = auth.decode(req.headers.authorization);
    if (!isAdmin) {
      res.status(400);
      throw new Error('Only admin can add course(s).');
    }

    const newCourse = await Course.create(req.body);
    res.status(201).json(newCourse);
  } catch (err) {
    next(err);
  }
};

export const getAllCourses = async (req, res, next) => {
  try {
    const courses = await Course.find();
    res.status(200).json(courses);
  } catch (err) {
    next(err);
  }
};

export const getAllActive = async (req, res, next) => {
  try {
    const courses = await Course.find({ isActive: true });
    res.status(200).json(courses);
  } catch (err) {
    next(err);
  }
};

export const getCourse = async (req, res, next) => {
  try {
    const course = await Course.findById(req.params.courseId);
    res.status(200).json(course);
  } catch (err) {
    next(err);
  }
};

export const updateCourse = async (req, res, next) => {
  try {
    const { isAdmin } = auth.decode(req.headers.authorization);
    if (!isAdmin) {
      res.status(400);
      throw new Error('Only admin can update course(s).');
    }

    const { name, description, price } = req.body;
    const updatedCourse = await Course.findByIdAndUpdate(
      req.params.courseId,
      { name, description, price },
      { new: true }
    );
    res.status(200).json(updatedCourse);
  } catch (err) {
    next(err);
  }
};

export const archiveCourse = async (req, res, next) => {
  try {
    // const { isAdmin } = auth.decode(req.headers.authorization);
    // if (!isAdmin) {
    //   res.status(400);
    //   throw new Error('Only admin can archive course(s).');
    // }

    const archivedCourse = await Course.findByIdAndUpdate(
      req.params.courseId,
      { isActive: false },
      { new: true }
    );
    res.status(200).json(archivedCourse);
  } catch (err) {
    next(err);
  }
};
