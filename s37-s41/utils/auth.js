import jwt from 'jsonwebtoken';

const secret = 'CourseBookingAPI';

export const createAccessToken = user => {
  const { _id: id, email, isAdmin } = user;
  const payload = { id, email, isAdmin };
  return jwt.sign(payload, secret, {});
};

export const verify = (req, res, next) => {
  let token = req.headers.authorization;
  if (!token) {
    res.status(401);
    throw new Error('Access denied. No token provided.');
  }

  try {
    token = token.slice(7, token.length);
    const verifiedToken = jwt.verify(token, secret);
    if (!verifiedToken) return;
    next();
  } catch (err) {
    res.status(400);
    throw new Error('Invalid token.');
  }
};

export const decode = token => {
  if (!token) return;
  token = token.slice(7, token.length);

  const verifiedToken = jwt.verify(token, secret);
  if (!verifiedToken) return;
  return jwt.decode(token);
};
