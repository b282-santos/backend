//  Note: don't add a semicolon at the end of then().
//  Fetch answers must be inside the await () for each function.
//  Each function will be used to check the fetch answers.
//  Don't console log the result/json, return it.

const baseUrl = 'https://jsonplaceholder.typicode.com/todos';

const getSingleToDo = async () => {
  const res = await fetch(`${baseUrl}/1`);
  return await res.json();
};

const getAllToDo = async () => {
  const res = await fetch(baseUrl);
  const data = await res.json();
  return data.map(todo => todo.title);
};

const getSpecificToDo = async () => {
  const res = await fetch(`${baseUrl}/1`);
  return await res.json();
};

const createToDo = async () => {
  const newToDo = {
    userId: 1,
    title: 'Created To Do List Item',
    completed: false,
  };

  const options = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(newToDo),
  };

  const res = await fetch(baseUrl, options);
  return await res.json();
};

const updateToDo = async () => {
  const updatedToDo = {
    userId: 1,
    title: 'Updated To Do List Item',
    description: 'To update the my to do list with a different data structure',
    dateCompleted: 'Pending',
    status: 'Pending',
  };

  const options = {
    method: 'PUT',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(updatedToDo),
  };

  const res = await fetch(`${baseUrl}/1`, options);
  return await res.json();
};

const deleteToDo = async () => {
  return await fetch(`${baseUrl}/1`, { method: 'DELETE' });
};

// Do not modify
// For exporting to test.js
try {
  module.exports = {
    getSingleToDo: typeof getSingleToDo !== 'undefined' ? getSingleToDo : null,
    getAllToDo: typeof getAllToDo !== 'undefined' ? getAllToDo : null,
    getSpecificToDo:
      typeof getSpecificToDo !== 'undefined' ? getSpecificToDo : null,
    createToDo: typeof createToDo !== 'undefined' ? createToDo : null,
    updateToDo: typeof updateToDo !== 'undefined' ? updateToDo : null,
    deleteToDo: typeof deleteToDo !== 'undefined' ? deleteToDo : null,
  };
} catch (err) {}
