import express, { json, urlencoded } from 'express';
import { connect, Schema, model } from 'mongoose';
import { genSalt, hash } from 'bcrypt';

const app = express();

const database =
  'mongodb+srv://renzodsantos:admin123@wdc028-course-booking.gaiqe0t.mongodb.net/s35';
const port = process.env.PORT || 3001;

/* Connect to MongoDB */
connect(database, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() =>
    app.listen(port, () => console.log(`Listening on port ${port}...`))
  )
  .catch(err => console.error(err.message));

/* Middlewares */
app.use(json());
app.use(urlencoded({ extended: true }));

/* Models */
const userSchema = new Schema({
  username: { type: String, unique: true, required: true },
  password: { type: String, required: true },
});
const User = model('User', userSchema);

/* Routes */
app.post('/users', async (req, res) => {
  try {
    const { username, password } = req.body;
    if (!username || !password)
      return res
        .status(400)
        .send('BOTH username and password must be provided.');

    const user = await User.findOne({ username });
    if (user) return res.status(400).send('Duplicate username found.');

    const salt = await genSalt(10);
    const hashedPassword = await hash(password, salt);
    const newUser = await User.create({
      ...req.body,
      password: hashedPassword,
    });
    if (!newUser)
      return res.status(400).send('An error occured while creating new user.');

    res.status(201).send('New user registered.');
  } catch (err) {
    res.status(500).send(err.message);
  }
});
