/*
  Create functions which can manipulate our arrays.
*/

/*
  Important note: Don't pass the arrays as an argument to the function.
  The functions must be able to manipulate the current given arrays.
*/

let registeredUsers = [
  'James Jeffries',
  'Gunther Smith',
  'Macie West',
  'Michelle Queen',
  'Shane Miguelito',
  'Fernando Dela Cruz',
  'Akiko Yukihime',
];

let friendsList = [];

/*
 1. Create a function called register which will allow us to register into the registeredUsers list.
      - this function should be able to receive a string.
      - determine if the input username already exists in our registeredUsers array.
        -if it is, return the message:
          - "Registration failed. Username already exists!"
        -if it is not, add the new username into the registeredUsers array and return the message:
          - "Thank you for registering!"
      - invoke and register a new user.
      - outside the function log the registeredUsers array.
*/

const register = username => {
  const isUserRegistered = registeredUsers.includes(username);
  if (isUserRegistered) return 'Registration failed. Username already exists!';

  registeredUsers.push(username);
  return 'Thank you for registering!';
};

/*
  2. Create a function called addFriend which will allow us to add a registered user into our friends list.
      - this function should be able to receive a string.
      - determine if the input username exists in our registeredUsers array.
          - if it is, add the foundUser in our friendList array.
              -Then return the message with the following message:
              - "You have added <registeredUser> as a friend!"
          - if it is not, return the message:
              - "User not found."
      - invoke the function and add a registered user in your friendsList.
      - Outside the function log the friendsList array in the console.
*/

const addFriend = username => {
  const isUserRegistered = registeredUsers.includes(username);
  if (!isUserRegistered) return 'User not found.';

  if (friendsList.includes(username))
    return `You and ${username} are already friend!`;

  friendsList.push(username);
  return `You have added ${username} as a friend!`;
};

/*
  3. Create a function called displayFriends which will allow us to show/display the items in the friendList one by one on our console.
      - If the friendsList is empty return the message: 
          - "You currently have 0 friends. Add one first."
      - Invoke the function.
*/

const displayFriends = () => {
  if (!friendsList.length)
    return 'You currently have 0 friends. Add one first.';

  friendsList.forEach(friend => {
    console.log(friend);
  });
};

/*
  4. Create a function called displayNumberOfFriends which will display the amount of registered users in your friendsList.
      - If the friendsList is empty return the message:
        - "You currently have 0 friends. Add one first."
      - If the friendsList is not empty return the message:
        - "You currently have <numberOfFriends> friends."
      - Invoke the function
*/

const displayNumberOfFriends = () => {
  const totalFriends = friendsList.length;
  if (!totalFriends) return 'You currently have 0 friends. Add one first.';

  return `You currently have ${totalFriends} ${
    totalFriends > 1 ? 'friends' : 'friend'
  }.`;
};

/*
  5. Create a function called deleteFriend which will delete the last registeredUser you have added in the friendsList.
      - If the friendsList is empty return a message:
        - "You currently have 0 friends. Add one first."
      - Invoke the function.
      - Outside the function log the friendsList array.
*/

const deleteFriend = () => {
  if (!friendsList.length)
    return 'You currently have 0 friends. Add one first.';

  const deletedFriend = friendsList.pop();
  return `You have deleted ${deletedFriend} from you friends list.`;
};

/*
  Stretch Goal:

  Instead of only deleting the last registered user in the friendsList delete a specific user instead.
    -You may get the user's index.
    -Then delete the specific user with splice().
*/

const deleteSpecificFriend = username => {
  if (!friendsList.length)
    return 'You currently have 0 friends. Add one first.';

  const isUserRegistered = friendsList.includes(username);
  if (!isUserRegistered) return `User not found.`;

  const indexToDelete = friendsList.indexOf(username);
  friendsList.splice(indexToDelete, 1);

  // ! Delete friend using filter() method
  // friendsList = friendsList.filter(friend => friend !== username);

  return `You have deleted ${username} from you friends list.`;
};

// For exporting to test.js
try {
  module.exports = {
    registeredUsers:
      typeof registeredUsers !== 'undefined' ? registeredUsers : null,
    friendsList: typeof friendsList !== 'undefined' ? friendsList : null,
    register: typeof register !== 'undefined' ? register : null,
    addFriend: typeof addFriend !== 'undefined' ? addFriend : null,
    displayFriends:
      typeof displayFriends !== 'undefined' ? displayFriends : null,
    displayNumberOfFriends:
      typeof displayNumberOfFriends !== 'undefined'
        ? displayNumberOfFriends
        : null,
    deleteFriend: typeof deleteFriend !== 'undefined' ? deleteFriend : null,
  };
} catch (err) {}
