import { Router } from 'express';
import * as taskController from '../controllers/taskController.js';

const router = Router();

router.get('/:id', taskController.getTask);
router.put('/:id/complete', taskController.updateTask);

export default router;
