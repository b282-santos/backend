import { json, urlencoded } from 'express';
import taskRoute from '../routes/taskRoute.js';

export default app => {
  app.use(json());
  app.use(urlencoded({ extended: true }));

  app.use('/tasks', taskRoute);
  app.all('*', (req, res) => res.status(404).send('Page not found.'));

  app.use((err, req, res, next) => {
    const { message, stack } = err;
    res.status(res.statusCode).send({ message, stack });
  });
};
