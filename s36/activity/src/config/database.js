import { connect } from 'mongoose';

export default app => {
  const database =
    'mongodb+srv://renzodsantos:admin123@wdc028-course-booking.gaiqe0t.mongodb.net/s36';
  const port = process.env.PORT || 3001;

  connect(database, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() =>
      app.listen(port, () => {
        console.log('Connected to MongoDB');
        console.log(`Listening on port ${port}...`);
      })
    )
    .catch(err => console.error(err.message));
};
