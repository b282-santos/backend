import { isValidObjectId } from 'mongoose';
import Task from '../models/Task.js';

export const getTask = async (req, res, next) => {
  try {
    const { id: _id } = req.params;
    if (!isValidObjectId(_id)) {
      res.status(400);
      throw new Error('Invalid Task ID.');
    }

    const task = await Task.findOne({ _id });
    if (!task) {
      res.status(400);
      throw new Error('Task not found.');
    }
    res.status(200).json(task);
  } catch (err) {
    next(err);
  }
};

export const updateTask = async (req, res, next) => {
  try {
    const { id: _id } = req.params;
    if (!isValidObjectId(_id)) {
      res.status(400);
      throw new Error('Invalid Task ID.');
    }

    const task = await Task.findOne({ _id });
    if (!task) {
      res.status(400);
      throw new Error('Task not found.');
    }

    const updatedTask = await Task.findByIdAndUpdate(
      _id,
      { ...req.body, status: 'complete' },
      { new: true }
    );
    res.status(200).json(updatedTask);
  } catch (err) {
    next(err);
  }
};
