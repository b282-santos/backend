import { Schema, model } from 'mongoose';

const taskSchema = new Schema({
  name: String,
  status: { type: String, default: 'pending' },
});

export default model('Task', taskSchema);
