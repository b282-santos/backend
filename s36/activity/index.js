import express from 'express';
import database from './src/config/database.js';
import routes from './src/config/routes.js';

const app = express();

database(app);
routes(app);
