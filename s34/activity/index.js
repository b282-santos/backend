import express, { json, urlencoded } from 'express';

const app = express();

const port = process.env.PORT || 3000;
const users = [];

/* MIDDLEWARES */
app.use(json());
app.use(urlencoded({ extended: true }));

/* ROUTES */
app.get('/home', (req, res) => res.send('Welcome to the home page.'));

app.get('/users', (req, res) => res.json(users));

app.post('/users', (req, res) => {
  const { username, password } = req.body;
  if (!username || !password)
    return res.status(400).send('Username and password is required.');

  const user = users.find(user => user.username === username);
  if (user) return res.status(400).send('Username is already registered.');

  const newUser = { _id: users.length + 1, ...req.body };
  users.push(newUser);
  res.status(201).send(`User ${username} successfully registered.`);
});

app.delete('/users/:id', (req, res) => {
  const { id } = req.params;
  let message;

  if (!users.length) {
    res.status(400);
    message = 'No users found.';
  } else {
    const user = users.find(user => user._id === Number(id));
    if (!user) {
      res.status(400);
      message = 'User does not exist.';
    } else {
      const index = users.indexOf(user);
      users.splice(index, 1);
      message = `User ${user.username} has been deleted.`;
    }
  }

  res.send(message);
});

app.all('*', (req, res) => res.status(404).send('Page not found.'));

app.listen(port, () => console.log(`Listening on port ${port}...`));
