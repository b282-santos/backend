// console.log("Hello World");

// Objective 1
let string = 'supercalifragilisticexpialidocious';
console.log(string);
let filteredString = '';

for (let i = 0; i < string.length; i++) {
  const char = string[i].toLowerCase();
  const vowels = ['a', 'e', 'i', 'o', 'u'];

  if (!vowels.includes(char)) filteredString += string[i];
}

console.log(filteredString);

// Add code here

// Do not modify
// For exporting to test.js
// Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try {
  module.exports = {
    filteredString:
      typeof filteredString !== 'undefined' ? filteredString : null,
  };
} catch (err) {}
