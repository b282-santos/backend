const { createServer } = require('http');

const app = createServer((req, res) => {
  const { url, method } = req;

  res.writeHead(200, { 'Content-Type': 'text/plain' });

  if (url === '/' && method === 'GET')
    res.write('Welcome to Booking System');

  if (url === '/profile' && method === 'GET')
    res.write('Welcome to your profile!');

  if (url === '/courses' && method === 'GET')
    res.write('Heres our courses available');

  if (url === '/addcourse' && method === 'POST')
    res.write('Add a course to our resources');

  if (url === '/updatecourse' && method === 'PUT')
    res.write('Update a course to our resources');

  if (url === '/archivecourses' && method === 'DELETE')
    res.write('Archieve courses to our resources');

  res.end();
});

// Do not modify
// Make sure to save the server in variable called app
if (require.main === module) {
  app.listen(4000, () => console.log(`Server running at port 4000`));
}

module.exports = app;
