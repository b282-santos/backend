// console.log("Hello World");

// Strictly Follow the property names and spelling given in the google slide instructions.
// Note: Do not change any variable and function names.
// All variables and functions to be checked are listed in the exports.

// Create an object called trainer using object literals
const trainer = {};

// Initialize/add the given object properties and methods

// Properties
trainer.name = 'Ash Ketchum';
trainer.age = 10;
trainer.pokemon = ['Pikachu', 'Charizard', 'Squirtle', 'Bulbasaur'];
trainer.friends = { hoenn: ['May', 'Max'], kanto: ['Brock', 'Misty'] };

// Methods
trainer.talk = function (pokemonIndex = 0) {
  return `${this.pokemon[pokemonIndex]}! I choose you!`;
};

// Check if all properties and methods were properly added
console.log(trainer);

// Access object properties using dot notation
console.log('Result of dot notation:');
console.log(trainer.name);

// Access object properties using square bracket notation
console.log('Result of square bracket notation:');
console.log(trainer['pokemon']);

// Access the trainer "talk" method
console.log('Result of talk method:');
console.log(trainer.talk());

// Create a constructor function called Pokemon for creating a pokemon
function Pokemon(name, level) {
  this.name = name;
  this.level = level;
  this.health = level * 2;
  this.attack = level;
  this.tackle = enemy => {
    const enemyHealth = enemy.health - this.attack;
    enemy.health = enemyHealth < 1 ? 0 : enemyHealth;

    console.log(`${this.name} tackled ${enemy.name}`);
    console.log(`${enemy.name}'s health is now reduced to ${enemy.health}`);

    if (enemy.health < 1) enemy.faint();
  };
  this.faint = () => {
    console.log(this.name, 'fainted');
  };
}

// Create/instantiate a new pokemon
const pikachu = new Pokemon('Pikachu', 12);
console.log(pikachu);

// Create/instantiate a new pokemon
const geodude = new Pokemon('Geodude', 8);
console.log(geodude);

// Create/instantiate a new pokemon
const mewtwo = new Pokemon('Mewtwo', 100);
console.log(mewtwo);

// Invoke the tackle method and target a different object
geodude.tackle(pikachu);
console.log(pikachu);

// Invoke the tackle method and target a different object
mewtwo.tackle(geodude);
console.log(geodude);

// Do not modify
// For exporting to test.js
// Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try {
  module.exports = {
    trainer: typeof trainer !== 'undefined' ? trainer : null,
    Pokemon: typeof Pokemon !== 'undefined' ? Pokemon : null,
  };
} catch (err) {}
